import argparse
import pickle
 
import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.linear_model import ElasticNet
from sklearn.impute import SimpleImputer
import sys
import warnings
import logging
 
 
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-train_path',
                        action="store",
                        dest="train_path",
                        required=True)
    parser.add_argument('-path_pkl',
                        action="store",
                        dest="path_pkl")
    args = parser.parse_args()
    return args
 
 

  
logging.basicConfig(level=logging.WARN)
logger = logging.getLogger(__name__)
  
  
def eval_metrics(actual, pred):
    rmse = np.sqrt(mean_squared_error(actual, pred))
    mae = mean_absolute_error(actual, pred)
    r2 = r2_score(actual, pred)
    return rmse, mae, r2
     
def prepare_train(dataset: pd.DataFrame):
    """
    Преобразование данных
    """
    temp=dataset[['Age']]
    dataset.drop(['Age'],inplace=True,axis=1)
 
    my_imputer=SimpleImputer()
    imputed_temp = pd.DataFrame(my_imputer.fit_transform(temp))
    imputed_temp.columns = temp.columns
 
    dataset=pd.concat([dataset,imputed_temp],axis=1)
 
    dataset['Embarked'].fillna(dataset['Embarked'].mode(),inplace=True)
    dummy1=pd.get_dummies(dataset[['Embarked']])
    dataset.drop(['Cabin', 'Embarked'] ,axis=1,inplace=True)
    dataset=pd.concat([dataset,dummy1],axis=1)
    dataset.drop(['Name',
                'PassengerId',
                'Sex',
                'Ticket'],
               axis=1,
               inplace=True)
    return dataset
  
  
def model(path_train):
    warnings.filterwarnings("ignore")
    np.random.seed(40)
  
    # Read the wine-quality csv file from the URL
    try:
        dataset = pd.read_csv(path_train, sep=",")
    except Exception as e:
        logger.exception(
            "Error: %s", e
        )
         
    dataset = prepare_train(dataset)
  
    # Split the data into training and test sets. (0.75, 0.25) split.
    train, test = train_test_split(dataset)
  
    train_x = train.drop(["Survived"], axis=1)
    test_x = test.drop(["Survived"], axis=1)
    train_y = train[["Survived"]]
    test_y = test[["Survived"]]
  
    alpha = 0.5
    l1_ratio = 0.5
  
    lr = ElasticNet(alpha=alpha, l1_ratio=l1_ratio, random_state=42)
    lr.fit(train_x, train_y)
  
    predicted_qualities = lr.predict(test_x)
  
    (rmse, mae, r2) = eval_metrics(test_y, predicted_qualities)
  
    print("Elasticnet model (alpha=%f, l1_ratio=%f):" % (alpha, l1_ratio))
    print("  RMSE: %s" % rmse)
    print("  MAE: %s" % mae)
    print("  R2: %s" % r2)
    
    return lr
 

def train_female(path_train):
    lr = model(path_train + "/train_female.csv")
     
    with open(args.path_pkl + "/model_female.pkl", 'wb') as fd:
        pickle.dump(lr, fd)

def train_male(path_train):
    lr = model(path_train + "/train_male.csv")
     
    with open(args.path_pkl + "/model_male.pkl", 'wb') as fd:
        pickle.dump(lr, fd)
 
if __name__ == "__main__":
    args = get_args()
     
    train_female(args.train_path)
    train_male(args.train_path)