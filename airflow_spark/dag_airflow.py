from airflow.models import DAG
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator
from airflow.utils.dates import days_ago

args = {
    'owner': 'Airflow',
}

with DAG(
    dag_id='example_spark_operator',
    default_args=args,
    schedule_interval=None,
    start_date=days_ago(2),
    tags=['spark'],
) as dag:

    python_submit_job = SparkSubmitOperator(
        application="/opt/spark/examples/src/main/python/pi.py",
        task_id="python_job",
        conn_id='spark_default',
        executor_cores=2,
        executor_memory='2g',
        driver_memory='2g',
        dag=dag
    )

python_submit_job
